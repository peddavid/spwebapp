'use strict'

const router = require('koa-router')()

const spCredentials = require('../secrets')
const spService = require('../spService')(spCredentials)

router.get('/search', async ctx => {
  const { type, q, offset, limit } = ctx.query
  const result = await spService.search(type, q, offset, limit)
  console.log(result)
  await ctx.render('search', result)
})

router.get('/artists/:id', ctx => {

})

module.exports = router.routes()
