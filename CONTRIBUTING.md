# Contributing to Spotify Web Application

:+1::tada: First off, thanks for taking the time to contribute! :tada::+1:

We are open to, and grateful for, any contributions made by the community.

By contributing to Spotify Web Application, you agree to abide by the [code of conduct](CODE_OF_CONDUCT.md) and that your contributions will be licensed under its [license](LICENSE).

## Building

## Code Conventions

[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

## Issues

## Pull Requests

[How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)
