'use strict'

const { SimpleArtist } = require('./Artist')
const Paging = require('./Paging')

module.exports = {
  SimpleAlbum: function ({id, name, album_type, images, artists, uri}) {
    // simpleArtists
    return {
      id,
      name,
      albumType: album_type,
      images,
      artists: artists.map(SimpleArtist),
      spotifyUri: uri
    }
  },
  Album: function (spAlbum) {
    const {
      id,
      name,
      genres,
      release_date,
      album_type,
      images,
      artists,
      tracks,
      uri
    } = spAlbum
    // simpleTracks inside paging object
    return {
      id,
      name,
      genres,
      releaseDate: release_date,
      albumType: album_type,
      images,
      artists: artists.map(SimpleArtist),
      tracks: Paging(tracks),
      spotifyUri: uri
    }
  }
}
