module.exports = function Paging (spPaging) {
  const {items, offset, limit, total, previous, next} = spPaging
  return {
    items,
    offset,
    limit,
    total,
    previous,
    next
  }
}
