'use strict'

module.exports = {
  SimpleArtist: function ({id, name, uri}) {
    return {
      id,
      name,
      spotifyUri: uri
    }
  },
  Artist: function ({id, name, genres, images, popularity, followers, uri}) {
    return {
      id,
      name,
      genres,                           // Only in Artist
      images,                           // Only in Artist
      popularity,                       // Only in Artist
      totalFollowers: followers.total,  // Only in Artist
      spotifyUri: uri
    }
  }
}
